#  Project: IPK project 2
#  File:    Makefile
#  Date:    29.4.2015
#  Author:  Martin Cernek, xcerne01@stud.fit.vutbr.cz

SERVER=ipkhttpserver.py
CLIENT=ipkhttpclient.py
TESTSCRIPT=run_test.sh

all:
	chmod +x $(SERVER)
	chmod +x $(CLIENT)

.PHONY: clean test pack

clean:
	rm -rf cl_logs serv_logs *.payload *.header

test: all
	chmod +x $(TESTSCRIPT)
	bash $(TESTSCRIPT)

pack:
	tar -zcvf xcerne01.tgz $(SERVER) $(CLIENT) www README Makefile run_test.sh test
