# Zadání

Vytvořte jednoduchý HTTP server a klient, který bude volitelně využívat Chunked transfer encoding. Server zpřístupní obsah v adresáři ./www
se základní podporou Content-Type: text/plain.
K serveru bude možné připojit se přes běžný webový prohlížeč, který zobrazí požadovaný obsah.
 
Spuštění serveru bude následující: ./ipkhttpserver [-h] [-c chunk-max-size] [-p port] [-t time]

* -h --help zobrazí nápovědu
* -c --chunk-max-size - maximální velikost dat v chunku
* -p --port TCP port, na kterém bude server naslouchat 
* -t --min-chunk-time (ms) minimalni cas cekani pred odeslanim dalsiho chunku, maximalni cas 2x min-chunk-time

Server se korektně vypne po obdržení signálu **SIGINT**.

Spuštění klientu bude následující: ./ipkhttpclient [-h] URI

* -h --help zobrazí nápovědu
* URI - rfc3986
 
Aplikace klient uloží do souboru "ipkResp-YYYY-MM-DD:HH:MM:SS.header" hlavičku, do souboru "ipkResp-YYYY-MM-DD:HH:MM:SS.payload" rekonstruovaný objekt.

Všechna příchozí komunikace se bude logovat do souboru "ipkHttp(Server|Client)-YYYY-MM-DD:HH:MM:SS.in.log", odchozí "ipkHttp(Server|Client)-YYYY-MM-DD:HH:MM:SS.out.log", kde časové razítko se určí při spuštění programu.

Při požadavku na neexistující dokument server odpoví chybou HTTP 404.

# Doplnujíci požadavky

Implementační jazyk C/C++, Python, Perl, Bash, Assembler (x86), Brainfuck, Whitepace, Lisp, Prolog, Haskel, ObjectiveC, C#, MATLAB, Maple a jejich kombinace. Je možné využít libovolnou knihovnu, která byla schválena na fóru předmětu. 
Aplikace bude povinně obsahovat README s popisem implementace a příklady spuštění. Pokud nestihnete implementovat část funkcionality, bude to taktéž povinně zmíněno v README.

Přeložení aplikace zajistí Makefile, který při překladu stáhne požadované schválené knihovny a zajistí překlad bez manuálního zásahu uživatele. Překlad musí být úspěšný bez chyb a varování! Makefile bude dále povinně implementovat PHONY clean, test a pack s následující funkcionalitou:


* make clean - smaže vše kromě zdrojových a testovacích souborů
* make test - spustí navrženou sadu testů a vypíše výsledek (i postup) na STDOUT
* make pack - vytvoří archiv k odevzdání

Je vyžadováno, aby sada testů pokrývala kompletně implementovanou funkcionalitu.
Projekt spolu s testovacími daty bude odevzdán v komprimovaném formátu xlogin00.tgz (GZIP komprimovaný TAR archiv). Testování proběhne na virtuálním stroji Xubuntu 14.04 x86 (http://nes.fit.vutbr.cz/ipk/IPK2015-ubuntu.ova). Očekává se čitelný a přiměřeně komentovaný zdrojový kód.