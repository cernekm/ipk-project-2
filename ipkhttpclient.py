#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import getopt
import socket
import os
import datetime
import re
import urllib.parse

def parse_response(response):
    split_response = response.split("\r\n", 1)
    status_line = split_response[0].split(" ")
    header_dict = { tmp.split(":", 1)[0].lower():tmp.split(":", 1)[1].strip().lower() for tmp in split_response[1].split("\r\n") if tmp }
    try:
        content_length = int(header_dict["content-length"])
    except KeyError:
        content_length = None
    chunked = False
    try:
        if header_dict["transfer-encoding"] == "chunked":
            chunked = True
    except KeyError:
        pass
    try:
        encoding = re.search(r"charset=(.*)$", header_dict["content-type"]).group(1)
    except:
        encoding = "utf-8"
    return status_line[0], status_line[1], status_line[2], content_length, chunked, encoding.lower()


def main(argv):
    
    start_time = datetime.datetime.now().strftime("%Y-%m-%d:%H:%M:%S")
    port = 80

    try:
        opts, args = getopt.getopt(argv, "h", ["help"])
    except:
        sys.stderr.write("ipkhttpclient: invalid options\nTry '-h' for more information.\n")
        return 1
    if len(opts) == 1:
        if args or opts[0][0] not in argv:
            sys.stderr.write("ipkhttpclient: invalid options\nTry '-h' for more information.\n")
            return 1
        print("Napoveda")
        return 0
    if len(args) != 1:
        sys.stderr.write("ipkhttpclient: invalid options\nTry '-h' for more information.\n")
        return 1
    
    # Spracovanie URI
    URI = urllib.parse.urlparse(args[0])

    #print(URI.port)
    #return 0

    if URI.scheme != "http":
        sys.stderr.write("ipkhttpclient: invalid scheme of URI\n")
        return 2
    if not URI.hostname:
        sys.stderr.write("ipkhttpclient: invalid hostname of URI\n")
        return 2

    # Nastavenie portu
    if URI.port:
        port = URI.port

    log_dir = "logs"
    os.makedirs(log_dir, exist_ok = True)
    recv_log_filename = log_dir + "/ipkhttpClient-" + start_time + ".in.log"
    send_log_filename = log_dir + "/ipkHttpClient-" + start_time + ".out.log"
    header_filename =  "ipkResp-" + start_time + ".header"
    payload_filename =  "ipkResp-" + start_time + ".payload"

    # Otvorenie suborov
    try:
        recv_log_file = open(recv_log_filename, "w")
    except PermissionError:
        sys.stderr.write("ipkhttpclient: unable to create log file\n")
        return 3
    try:
        send_log_file = open(send_log_filename, "w")
    except PermissionError:
        sys.stderr.write("ipkhttpclient: unable to create log file\n")
        recv_log_file.close()
        return 3
    try:        
        header_file = open(header_filename, "w")
    except PermissionError:
        sys.stderr.write("ipkhttpclient: unable to create header file\n")
        recv_log_file.close()
        send_log_file.close()
        return 3
    try:
        payload_file = open(payload_filename, "w")
    except PermissionError:
        sys.stderr.write("ipkhttpclient: unable to create payload file\n")
        recv_log_file.close()
        send_log_file.close()
        header_file.close()
        return 3


    # Vytvorenie socketu
    try:
        socket_handle = socket.socket()
    except:
        sys.stderr.write("ipkhttpclient: unable to create socket\n")
        recv_log_file.close()
        send_log_file.close()
        header_file.close()
        payload_file.close()
        return 10
    

    # Naviazanie socketu na port
    try:
        socket_handle.connect((URI.hostname, port)) #
    except:
        sys.stderr.write("ipkhttpclient: unable to connect\n")
        recv_log_file.close()
        send_log_file.close()
        header_file.close()
        payload_file.close()
        socket_handle.close()
        return 11
        
    request = "GET " + URI.path + " HTTP/1.1\r\n" + \
              "Host: " + URI.hostname + ":" + str(port) + "\r\n"  + \
              "Connection: close\r\n" + \
              "Content-Type: text/plain\r\n" + \
              "\r\n"

    # Poslanie poziadavku
    socket_handle.send(request.encode())
    send_log_file.write(request)
    send_log_file.close()

    # Prijatie spravy
    response = b""
    try:
        while True:
            byte = socket_handle.recv(1);
            response += byte
            if response[-4:].decode() == "\r\n\r\n":
                break
    except ConnectionResetError:
        sys.stderr.write("ipkhttpclient: connection reset by peer\n")
        recv_log_file.close()
        header_file.close()
        payload_file.close()
        socket_handle.close()
        return 11

    recv_log_file.write(response.decode())
    header_file.write(response.decode().split("\r\n",1)[1])
    header_file.close()

    version, status_code, status, content_length, chunked, encoding = parse_response(response.decode())

    if version != "HTTP/1.1":
        sys.stderr.write("ipkhttpclient: unsupported version http\n")
        recv_log_file.close()
        payload_file.close()
        socket_handle.close()
        return 12

    if status_code != "200":
        recv_log_file.close()
        payload_file.close()
        socket_handle.close()
        return 0

    if chunked:
        bchunk_length = b""
        chunk = b""
        while True:
            while bchunk_length[-2:] != b"\r\n":
                bchunk_length += socket_handle.recv(1)
            recv_log_file.write(bchunk_length.decode(encoding))
            chunk_length = int(bchunk_length[:-2].decode(encoding), 16)
            print(chunk_length)
            bchunk_length = b""
            chunk = socket_handle.recv(chunk_length + 2, socket.MSG_WAITALL)
            print("real ", len(chunk))
            recv_log_file.write(chunk.decode(encoding))
            payload_file.write(chunk[:-2].decode(encoding))
            if chunk_length == 0:
                break
    elif content_length:
        content = b""
        content = socket_handle.recv(content_length + 2, socket.MSG_WAITALL)
        recv_log_file.write(content.decode(encoding))
        payload_file.write(content[:-2].decode(encoding))
    else:
        content = b""
        while True:
            data = socket_handle.recv(1024)
            if not data:
                break
            content += data
        recv_log_file.write(content.decode(encoding))
        payload_file.write(content[:-2].decode(encoding))

    recv_log_file.close()
    payload_file.close()
    socket_handle.close()

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
