#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import getopt
import socket
import datetime
import os
import re
import random
import time

class HTTPServer(object):

    def __init__(self, port, max_chunk_size=1024, min_chunk_time=0, content_folder="www" , log_folder="logs"):
        self.port = port
        self.max_chunk_size = max_chunk_size
        self.min_chunk_time = min_chunk_time
        self.content_folder = content_folder
        self.log_folder = log_folder
        self.recv_log_file = None
        self.send_log_file = None
        self.serv_socket_handle = None

        os.makedirs(self.log_folder, exist_ok=True)
        os.makedirs(self.content_folder, exist_ok=True)

    def _generate_response(self, response_type, size=None):
        if response_type == "nf":
            return "HTTP/1.1 404 Not found\r\n" + \
                   "Connection: close\r\n" + \
                   "Content-Length: 13\r\n" + \
                   "Content-Type: text/plain\r\n" + \
                   "\r\n" + \
                   "404 Not found\r\n"
        elif response_type == "br":
            return "HTTP/1.1 400 Bad Request\r\n" + \
                   "Connection: close\r\n" + \
                   "Content-Length: 15\r\n" + \
                   "Content-Type: text/plain\r\n" + \
                   "\r\n" + \
                   "400 Bad Request\r\n"
        elif response_type == "ni":
            return "HTTP/1.1 501 Not Implemented\r\n" + \
                   "Connection: close\r\n" + \
                   "Content-Length: 19\r\n" + \
                   "Content-Type: text/plain\r\n" + \
                   "\r\n" + \
                   "501 Not Implemented\r\n"
        elif response_type == "head":
            return "HTTP/1.1 200 OK\r\n" + \
                   "Connection: close\r\n" + \
                   "Content-Length: " + str(size) + "\r\n" + \
                   "Content-Type: text/plain\r\n" + \
                   "\r\n"
        elif response_type == "get":
            return "HTTP/1.1 200 OK\r\n" + \
                   "Connection: close\r\n" + \
                   "Transfer-Encoding: chunked\r\n" + \
                   "Content-Type: text/plain\r\n" + \
                   "\r\n"

    def _parse_request(self, request):
        split_request = request.decode().split("\r\n", 1)
        status_line = split_request[0].split(" ")
        return status_line[0], status_line[1], status_line[2], bool(re.match(r"Host: .+\r", split_request[1]))

    def run(self):
        start_time = datetime.datetime.now().strftime("%Y-%m-%d:%H:%M:%S")
        recv_log_filename = self.log_folder + "/ipkHttpServer-" + start_time + ".in.log"
        send_log_filename = self.log_folder + "/ipkHttpServer-" + start_time + ".out.log"
        try:
            self.recv_log_file = open(recv_log_filename, "w")
        except PermissionError:
            sys.stderr.write("ipkhttpserver: unable to create log file\n")
            return 3
        try:
            self.send_log_file = open(send_log_filename, "w")
        except PermissionError:
            sys.stderr.write("ipkhttpserver: unable to create log file\n")
            return 3

        # Vytvorenie socketu
        try:
            self.serv_socket_handle = socket.socket()
        except:
            sys.stderr.write("ipkhttpserver: unable to create socket\n")
            return 10

        # Nastavenia socketu
        self.serv_socket_handle.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        
        # Naviazanie socketu na port
        try:
            self.serv_socket_handle.bind(("",self.port))
        except:
            sys.stderr.write("ipkhttpserver: unable to bind socket to port\n")
            return 11

        # Priprava na akceptovanie poziadavkov
        self.serv_socket_handle.listen(1)

        # Spustenie serveru
        while True:
            try:
                # Akceptovanie poziadavku
                cl_socket_handle, cl_address = self.serv_socket_handle.accept()
                
                # Prijatie spravy
                request = b""
                while True:
                    byte = cl_socket_handle.recv(1);
                    request += byte
                    if request[-4:].decode() == "\r\n\r\n" or not byte:
                        break
                if not request:
                    continue

                # Zapis do logovacieho suboru
                self.recv_log_file.write(request.decode())

                # Spracovanie poziadavku
                try:
                    method, req_filename, version, host = self._parse_request(request)
                except IndexError:
                    #400 Bad Request
                    response = self._generate_response("br")
                    cl_socket_handle.send(response.encode())
                    self.send_log_file.write(response)
                    cl_socket_handle.close()
                    continue


                if version == "HTTP/1.1" and not host:
                    #400 Bad Request
                    response = self._generate_response("br")
                    cl_socket_handle.send(response.encode())
                    self.send_log_file.write(response)
                elif method != "GET" and method != "HEAD":
                    #501 Not Implemented
                    response = self._generate_response("ni")
                    cl_socket_handle.send(response.encode())
                    self.send_log_file.write(response)
                else:
                    # osetrenie relativnej cesty !!!!
                    filesize = None
                    try:
                        filesize = os.path.getsize(self.content_folder + req_filename)
                    except (FileNotFoundError, PermissionError):
                        #404 Not Found
                        response = self._generate_response("nf")
                        cl_socket_handle.send(response.encode())
                        self.send_log_file.write(response)

                    if filesize:
                        if method == "HEAD":
                            # posle sa hlavicka
                            response = self._generate_response("head", filesize)
                            cl_socket_handle.send(response.encode())
                            self.send_log_file.write(response)
                        elif method == "GET":
                            if filesize < self.max_chunk_size:
                                # posle sa cela
                                response = self._generate_response("head", filesize)
                                req_file = open(self.content_folder + req_filename, "rb")
                                req_content = req_file.read()
                                response = response.encode() + req_content + b"\r\n"
                                cl_socket_handle.send(response)
                                self.send_log_file.write(response.decode())
                                req_file.close()
                            else:
                                #posle sa v chunkoch
                                response = self._generate_response("get")
                                cl_socket_handle.send(response.encode())
                                self.send_log_file.write(response)
                                req_file = open(self.content_folder + req_filename, "rb")
                                while True:
                                    read_content = req_file.read(self.max_chunk_size)
                                    if not read_content:
                                        chunk_end = "0\r\n\r\n"
                                        cl_socket_handle.send(chunk_end.encode())
                                        self.send_log_file.write(chunk_end)
                                        break
                                    chunk = "{:x}\r\n".format(len(read_content)).encode() + read_content + b"\r\n"
                                    cl_socket_handle.send(chunk)
                                    self.send_log_file.write(chunk.decode())
                                    # Minimalny cas do poslania dalsieho chunku
                                    time.sleep(random.randint(self.min_chunk_time, 2 * self.min_chunk_time) / 1000.0)
                
                cl_socket_handle.close()

            except KeyboardInterrupt:
                raise KeyboardInterrupt

    def stop(self):
        try:
            self.recv_log_file.close()
            self.send_log_file.close()
            self.serv_socket_handle.close()
        except AttributeError:
            pass



def main(argv):
    
    port = None
    pflag = False
    chunk_max_size = 1024 # default
    cflag = False
    min_chunk_time = 0 # default
    tflag = False

    try:
        opts, args = getopt.getopt(argv, "hp:c:t:", ["help", "port=", "chunk-max-size=", "min-chunk-time="])
    except:
        sys.stderr.write("ipkhttpserver: invalid options\nTry '-h' for more information.\n")
        return 1
    if args:
        sys.stderr.write("ipkhttpserver: invalid options\nTry '-h' for more information.\n")
        return 1
    for opt, value in opts:
        if opt in [ "-h", "--help" ]:
            if len(argv) != 1:
                sys.stderr.write("ipkhttpserver: invalid options\nTry '-h' for more information.\n")
                return 1
            print("Napoveda")
            return 0
        elif opt in [ "-p", "--port"]:
            if pflag:
                sys.stderr.write("ipkhttpserver: invalid options\nTry '-h' for more information.\n")
                return 1
            pflag = True
            port = value
        elif opt in [ "-c", "--chunk-max-size"]:
            if cflag:
                sys.stderr.write("ipkhttpserver: invalid options\nTry '-h' for more information.\n")
                return 1
            cflag = True
            chunk_max_size = value
        elif opt in [ "-t", "--min-chunk-time"]:
            if tflag:
                sys.stderr.write("ipkhttpserver: invalid options\nTry '-h' for more information.\n")
                return 1
            tflag = True
            min_chunk_time = value
    # zabranenie oddelenia znakom = 
    if len(opts) * 2 != len(argv):
        sys.stderr.write("ipkhttpserver: invalid options\nTry '-h' for more information.\n")
        return 1
    if not pflag:
        sys.stderr.write("ipkhttpserver: option '-p|--port' is required\n")
        return 1

    try:
        port = int(port)
    except:
        sys.stderr.write("ipkhttpserver: invalid number of port\n")
        return 1
    if port <= 0 or port >= 65535:
        sys.stderr.write("ipkhttpserver: invalid number of port\n")
        return 1

    try:
        chunk_max_size = int(chunk_max_size)
    except:
        sys.stderr.write("ipkhttpserver: invalid maximal size of chunk\n")
        return 1
    if chunk_max_size <= 0:
        sys.stderr.write("ipkhttpserver: invalid maximal size of chunk\n")
        return 1

    try:
        min_chunk_time = int(min_chunk_time)
    except:
        sys.stderr.write("ipkhttpserver: invalid minimal chunk time\n")
        return 1
    if min_chunk_time < 0:
        sys.stderr.write("ipkhttpserver: invalid minimal chunk time\n")
        return 1


    server = HTTPServer(port, chunk_max_size, min_chunk_time)

    try:
        return server.run()
    except KeyboardInterrupt:
        server.stop()

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
