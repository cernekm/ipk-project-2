#!/bin/bash

# Vymazanie starych suborov
rm -rf ./test/my-out/* 2>/dev/null >/dev/null

cd ./test/my-out

SERVER="ipkhttpserver.py"
CLIENT="ipkhttpclient.py"
TESTS="./../tests"
OUT="./../my-out"

# Nastavenie pocitadiel
DIFF_ERR=0
RETURN_ERR=0



#./../../$SERVER --port 2222 &
#SERVER_PID=$!
#kill -9 $SERVER_PID 2>/dev/null

#touch $TESTS/$test/$test
#mkdir $test
#touch $test/$test.err

function run_test {
	echo "----------------------------------------------------------------------------"
	echo " RUNNING TEST $1"
	echo "----------------------------------------------------------------------------"

	pwd
	TEST="test-$1"
	mkdir $TEST
	cd $TEST
	pwd
	./../../../$2 $3 2>./$TEST.err
	#echo $?
	#RETURN=$?
	#echo -n $RETURN > ./$TEST.!!!

}



run_test 1 $CLIENT http://google.sk/index.html

